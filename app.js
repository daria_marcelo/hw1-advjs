// 1) Це наслідування від батьківського об'єкту за принципом копіювання його прототипу, в прототипі містяться властивості та методи об'єктів.
// 2) Для ініціалізації властивостей та методів батьківського класу.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name
    }
    get age() {
        return this._age
    }
    get salary() {
        return this._salary
    }

    set name(name) {
        this._name = name
    }
    set age(age) {
        this._age = age
    }
    set salary(salary) {
        this._salary = salary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang
    }

    get salary() {
        return (this._salary) * 3
    }
    get lang() {
        return this._lang;
    }

    set lang(lang) {
        this._lang = lang;
    }
}

let programmer1 = new Programmer("Daryna Marcelo", 24, 700, ['JS']);
let programmer2 = new Programmer("Tim Jackson", 32, 1700, ['JS', 'Python']);
let programmer3 = new Programmer("Anthony Joshua", 53, 20000, ['JS', 'Python', 'Java', 'C++']);
console.log(programmer1, programmer1.salary);
console.log(programmer2, programmer2.salary);
console.log(programmer3, programmer3.salary);
